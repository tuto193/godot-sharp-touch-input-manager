using Godot;
using System;

public class InputEventMultiScreenDrag : InputEventAction
{
	private Vector2 _position;
	public Vector2 Position { get => _position; set => _position = value; }

	private Vector2 _relative;
	public Vector2 Relative { get => _relative; set => _relative = value; }

	private Vector2 _speed;
	public Vector2 Speed { get => _speed; set => _speed = value; }


	private Vector2 GetEventsPropertyAverage(Godot.Collections.Dictionary events, string property)
	{
		Vector2 sum = new Vector2();
		foreach(InputEventScreenTouch e in events.Values )
		{
			sum += (Vector2)e.Get(property);
		}
		return sum / (int)events.Count;
	}


	public InputEventMultiScreenDrag( Godot.Collections.Dictionary dict )
	{
		if( dict.Contains("position") )
		{
			this.Position = (Vector2)dict["Position"];
			this.Relative= (Vector2)dict["Relative"];
			this.Speed = (Vector2)dict["Speed"];
		}
		else
		{
			this.Position = GetEventsPropertyAverage(dict, "Position");
			this.Relative= GetEventsPropertyAverage(dict, "Relative") / dict.Count;
			this.Speed = GetEventsPropertyAverage(dict, "Speed");
		}
	}

	new public string AsText()
	{
		string sentence = "InputEventMultiScreenDrag: position=" + Position.ToString();
		sentence += ", relative=" + Relative.ToString();
		sentence += ", speed = " + Speed.ToString();
		return sentence;

	}
}
