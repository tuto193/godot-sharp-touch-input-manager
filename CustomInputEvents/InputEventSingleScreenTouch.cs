using Godot;
using System;

public class InputEventSingleScreenTouch : InputEventAction
{
	private Vector2 _position;
	public Vector2 Position { get => _position; set => _position = value; }

	public InputEventSingleScreenTouch( Godot.Collections.Dictionary e )
	{
		this.Position = (Vector2)e["Position"];
		this.Pressed = (bool)e["Pressed"];
	}


	public InputEventSingleScreenTouch(InputEventScreenTouch e)
	{
		this.Position = e.Position;
		this.Pressed = e.Pressed;
	}

	new public string AsText()
	{
		string sentence = "InputEventSingleScreenTouch: position=" + Position.ToString();
		sentence += ", pressed= " + Pressed.ToString();
		return sentence;

	}
}
