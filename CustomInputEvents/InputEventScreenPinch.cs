using Godot;
using System;

public class InputEventScreenPinch : InputEventAction
{
	private Vector2 _position;
	public Vector2 Position { get => _position; set => _position = value; }

	private float _relative;
	public float Relative { get => _relative; set => _relative = value; }

	private float _speed;
	public float Speed { get => _speed; set => _speed = value; }

	private float _distance;
	public float Distance { get => _distance; set => _distance = value; }


	private Vector2 GetEventsPropertyAverage(Godot.Collections.Dictionary events, string property)
	{
		Vector2 sum = new Vector2(0, 0);
		foreach(InputEventScreenDrag e in events.Values )
		{
			sum += (Vector2)e.Get(property);
		}
		return sum / events.Count;
	}


	private float GetEventsPropertyAverageLength(Godot.Collections.Dictionary events, string property)
	{
		float sum = 0f;
		foreach(InputEventScreenDrag e in events.Values)
		{
			sum += ((Vector2)e.Get(property)).Length();
		}
		return sum / events.Count;
	}


	public InputEventScreenPinch( Godot.Collections.Dictionary dict )
	{
		if( dict.Contains("Position") )
		{
			this.Position = (Vector2)dict["Position"];
			this.Relative= (float)dict["Relative"];
			this.Speed = (float)dict["Speed"];
			this.Distance = (float)dict["Distance"];
		}
		else
		{
			this.Position = GetEventsPropertyAverage(dict, "Position");
			this.Speed = GetEventsPropertyAverageLength(dict, "Speed");
			this.Distance = 0f;
			this.Relative = 0f;
			foreach( InputEventScreenDrag e in dict.Values )
			{
				Distance += (e.Position - this.Position ).Length();
				Relative += (e.Position + (e.Relative / dict.Count) - this.Position).Length();
			}
			this.Relative -= this.Distance;
		}
	}

	new public string AsText()
	{
		string sentence = "InputEventScreenPinch: position=" + Position.ToString();
		sentence += ", relative=" + Relative.ToString();
		sentence += ", distance=" + Distance.ToString();
		sentence += ", speed = " + Speed.ToString();
		return sentence;

	}
}
