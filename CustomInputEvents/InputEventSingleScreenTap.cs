using Godot;
using System;

public class InputEventSingleScreenTap : InputEventAction
{
	private Vector2 _position;
	public Vector2 Position { get => _position; set => _position = value; }

	public InputEventSingleScreenTap( Godot.Collections.Dictionary e )
	{
		this.Position = (Vector2)e["Position"];
	}

	public InputEventSingleScreenTap( InputEventScreenTouch e)
	{
		this.Position = e.Position;
	}

	new public string AsText()
	{
		return "InputEventSingleScreenTap: position=" + Position.ToString();

	}

}
