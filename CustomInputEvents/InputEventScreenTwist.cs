using Godot;
using System;

public class InputEventScreenTwist : InputEventAction
{
	private Vector2 _position;
	public Vector2 Position { get => _position; set => _position = value; }

	private float _relative;
	public float Relative { get => _relative; set => _relative = value; }

	private float _speed;
	public float Speed { get => _speed; set => _speed = value; }

	private Vector2 GetEventsPropertyAverage(Godot.Collections.Dictionary events, string property)
	{
		Vector2 sum = new Vector2();
		foreach(InputEventScreenDrag e in events.Values )
		{
			sum += (Vector2)e.Get(property);
		}
		return sum / (int)events.Count;
	}


	private float GetEventsPropertyAverageLength(Godot.Collections.Dictionary events, string property)
	{
		float sum = 0f;
		foreach(InputEventScreenTouch e in events.Values)
		{
			sum += ((Vector2)e.Get(property)).Length();
		}
		return sum / events.Count;
	}


	public InputEventScreenTwist( Godot.Collections.Dictionary dict )
	{
		if( dict.Contains("Position") )
		{
			this.Position = (Vector2)dict["Position"];
			this.Relative= (float)dict["Relative"];
			this.Speed = (float)dict["Speed"];
		}
		else
		{
			this.Position = GetEventsPropertyAverage(dict, "Position");
			this.Speed= GetEventsPropertyAverageLength(dict, "Speed");
			this.Relative = 0f;
			foreach( InputEventScreenDrag e in dict.Values )
			{
				Relative += (e.Position - this.Position).AngleTo(e.Position + (e.Relative / dict.Count) - this.Position);
			}
			this.Relative /= dict.Count;
		}
	}

	new public string AsText()
	{
		string sentence = "InputEventScreenPinch: position=" + Position.ToString();
		sentence += ", relative=" + Relative.ToString();
		sentence += ", speed = " + Speed.ToString();
		return sentence;

	}
}
