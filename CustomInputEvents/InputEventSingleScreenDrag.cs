using Godot;
using System;

public class InputEventSingleScreenDrag : InputEventAction
{
	private Vector2 _position;
	public Vector2 Position { get => _position; set => _position = value; }

	private Vector2 _relative;
	public Vector2 Relative { get => _relative; set => _relative = value; }

	private Vector2 _speed;
	public Vector2 Speed { get => _speed; set => _speed = value; }

	public InputEventSingleScreenDrag( Godot.Collections.Dictionary e )
	{
		this.Position = (Vector2)e["Position"];
		this.Relative = (Vector2)e["Relative"];
		this.Speed = (Vector2)e["Speed"];
	}


	public InputEventSingleScreenDrag(InputEventScreenDrag e)
	{
		this.Position = e.Position;
		this.Relative = e.Relative;
		this.Speed = e.Speed;
	}


	new public string AsText()
	{
		string sentence = "InputEventSingleScreenDrag: position=" + Position.ToString();
		sentence += ", relative=" + Relative.ToString();
		sentence += ", speed = " + Speed.ToString();
		return sentence;

	}
}
