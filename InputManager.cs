using Godot;
using System;

public sealed class InputManager : Node
{
	// Properties
	private bool debug = false;
	private const float DRAG_STARTUP_TIME = 0.02f;
	private const float TOUCH_DELAY_TIME = 0.2f;

	// signals
	[Signal]
	public delegate void SingleTap();
	[Signal]
	public delegate void SingleTouch();
	[Signal]
	public delegate void SingleDrag();
	[Signal]
	public delegate void MultiDrag();
	[Signal]
	public delegate void Pinch();
	[Signal]
	public delegate void Twist();
	[Signal]
	public delegate void AnyGesture();

	// Control
	private InputEventMouse _last_mouse_press;
	private Godot.Collections.Dictionary _touches;
	private Godot.Collections.Dictionary _drags;

	private Timer _tap_delay_timer;
	public Timer TapDelayTimer { get => _tap_delay_timer; set => _tap_delay_timer = value; }

	private InputEventScreenTouch _only_touch;
	public InputEventScreenTouch OnlyTouch { get => _only_touch; set => _only_touch = value; }

	private Timer _drag_startup_timer;
	public Timer DragStartupTimer { get => _drag_startup_timer; set => _drag_startup_timer = value; }
	private bool _drag_enabled;

	// Enum
	private enum Gesture { PINCH, MULTI_DRAG, TWIST }


	// private static readonly Lazy<InputManager> lazy = new Lazy<InputManager>(()=> new InputManager() );


	// public static InputManager Instance{ get { return lazy.Value; } }


	public InputManager()
	{
		_touches = new Godot.Collections.Dictionary();
		_drags = new Godot.Collections.Dictionary();
		_tap_delay_timer = new Timer();
		_last_mouse_press = null;
		_only_touch = null;
		_drag_startup_timer = new Timer();
		_drag_enabled = false;
	}


	/// <summary>
	/// EmitSignal sig with the specified args
	/// </summary>
	/// <param name="_signal">Signal to be emitted.</param>
	/// <param name="e">Argument of the signal to be emitted.</param>
	private void Emit(string _signal, InputEventSingleScreenTap e)
	{
		if(debug)
		{
			GD.Print(e.AsText());
		}
		EmitSignal(nameof(AnyGesture), _signal, e);
		EmitSignal(_signal, e);
	}


	/// <summary>
	/// EmitSignal sig with the specified args
	/// </summary>
	/// <param name="_signal">Signal to be emitted.</param>
	/// <param name="e">Argument of the signal to be emitted.</param>
	private void Emit(string _signal, InputEventSingleScreenDrag e)
	{
		if(debug)
		{
			GD.Print(e.AsText());
		}
		EmitSignal(nameof(AnyGesture), _signal, e);
		EmitSignal(_signal, e);
	}


	/// <summary>
	/// EmitSignal sig with the specified args
	/// </summary>
	/// <param name="_signal">Signal to be emitted.</param>
	/// <param name="e">Argument of the signal to be emitted.</param>
	private void Emit(string _signal, InputEventSingleScreenTouch e)
	{
		if(debug)
		{
			GD.Print(e.AsText());
		}
		EmitSignal(nameof(AnyGesture), _signal, e);
		EmitSignal(_signal, e);
	}


	/// <summary>
	/// EmitSignal sig with the specified args
	/// </summary>
	/// <param name="_signal">Signal to be emitted.</param>
	/// <param name="e">Argument of the signal to be emitted.</param>
	private void Emit(string _signal, InputEventScreenTwist e)
	{
		if(debug)
		{
			GD.Print(e.AsText());
		}
		EmitSignal(nameof(AnyGesture), _signal, e);
		EmitSignal(_signal, e);
	}


	/// <summary>
	/// EmitSignal sig with the specified args
	/// </summary>
	/// <param name="_signal">Signal to be emitted.</param>
	/// <param name="e">Argument of the signal to be emitted.</param>
	private void Emit(string _signal, InputEventMultiScreenDrag e)
	{
		if(debug)
		{
			GD.Print(e.AsText());
		}
		EmitSignal(nameof(AnyGesture), _signal, e);
		EmitSignal(_signal, e);
	}


	/// <summary>
	/// EmitSignal sig with the specified args
	/// </summary>
	/// <param name="_signal">Signal to be emitted.</param>
	/// <param name="e">Argument of the signal to be emitted.</param>
	private void Emit(string _signal, InputEventScreenPinch e)
	{
		if(debug)
		{
			GD.Print(e.AsText());
		}
		EmitSignal(nameof(AnyGesture), _signal, e);
		EmitSignal(_signal, e);
	}


	/// <summary>
	/// Disables drag and stops the drag enabling timer.
	/// </summary>
	private void _CancelSingleDrag()
	{
		_drag_enabled = false;
		DragStartupTimer.Stop();
	}


	/// <summary>
	/// Checks if the gesture is a pinch.
	/// </summary>
	/// <param name="drags">the dict of actual textures happening.</param>
	/// <returns>The type of texture detected.</returns>
	private Gesture _IdentifyGesture(Godot.Collections.Dictionary drags)
	{
		Vector2 center = new Vector2();
		foreach(InputEventScreenTouch e in drags.Values)
		{
			center += e.Position;
		}
		center /= drags.Count;

		int sector = 0;
		foreach(InputEventScreenDrag e in drags.Values)
		{
			Vector2 adjusted_position = center - e.Position;
			float raw_angle = (adjusted_position.AngleTo(e.Relative) + ((float)Math.PI/4)) % Mathf.Tau;
			float adjusted_angle = raw_angle >= 0?raw_angle:raw_angle + Mathf.Tau;
			int e_sector = (int)Math.Floor(adjusted_angle / (Math.PI/2));
			if(sector == 0)
			{
				sector = e_sector;
			}
			else if(sector != e_sector)
			{
				sector = -1;
			}
		}
		switch(sector)
		{
			case -1:
				return Gesture.MULTI_DRAG;
			case 0:
				return Gesture.PINCH;
			case 1:
				return Gesture.TWIST;
			case 2:
				return Gesture.PINCH;
			case 3:
				return Gesture.TWIST;
			default:
				return Gesture.MULTI_DRAG;
		}
	}


	/// <summary>
	/// /Macro to add a timer t and connect its timeout to a func_name
	/// </summary>
	/// <param name="t">The timer to be connected.</param>
	/// <param name="func_name">The function to be associated with the timer.</param>
	private void _AddTimer(Timer t, string func_name)
	{
		t.OneShot = true;
		t.Connect("timeout", this, func_name);
		this.AddChild(t);
	}


	private bool _ComplexGestureInProgress()
	{
		return _touches.Count > 1;
	}


	private void _OnTapDelayTimerTimeout()
	{
		if(OnlyTouch != null && _touches.Count == 0)
		{
			Emit(nameof(SingleTap), new InputEventSingleScreenTap(OnlyTouch));
		}
	}


	private void _OnDragStartupTimeout()
	{
		_drag_enabled = !_ComplexGestureInProgress() && _drags.Count > 0;
	}


	// Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
		_AddTimer(TapDelayTimer, nameof(_OnTapDelayTimerTimeout));
		_AddTimer(DragStartupTimer, nameof(_OnDragStartupTimeout));
    }

	public override void _UnhandledInput(InputEvent @event)
	{
		// mouse to gesture
		if(@event is InputEventMouseButton event0)
		{
			if(event0.Pressed)
			{
				if(event0.ButtonIndex == (int)Godot.ButtonList.WheelDown)
				{
					Godot.Collections.Dictionary p_dict = new Godot.Collections.Dictionary();
					p_dict["Position"] = event0.Position;
					p_dict["Distance"] = 200f;
					p_dict["Relative"] = -40f;
					p_dict["Speed"] = 25f;
					Emit(nameof(Pinch), new InputEventScreenPinch(p_dict));
				}
				else if(event0.ButtonIndex == (int)Godot.ButtonList.WheelUp)
				{
					Godot.Collections.Dictionary p_dict = new Godot.Collections.Dictionary();
					p_dict["Position"] = event0.Position;
					p_dict["Distance"] = 200f;
					p_dict["Relative"] = 40f;
					p_dict["Speed"] = 25f;
					Emit(nameof(Pinch), new InputEventScreenPinch(p_dict));
				}
				_last_mouse_press = event0;
			}
			else
			{
				_last_mouse_press = null;
			}
		}
		else if(@event is InputEventMouseMotion event1)
		{
			if(_last_mouse_press != null)
			{
				if(((InputEventMouseButton)_last_mouse_press).ButtonIndex == (int)Godot.ButtonList.Middle)
				{
					Godot.Collections.Dictionary mult_drag_dict = new Godot.Collections.Dictionary();
					mult_drag_dict["Position"] = event1.Position;
					mult_drag_dict["Relative"] = event1.Relative;
					mult_drag_dict["Speed"] = event1.Speed;
					Emit(nameof(MultiDrag), new InputEventMultiScreenDrag(mult_drag_dict));
				}
				else if(((InputEventMouseButton)_last_mouse_press).ButtonIndex == (int)Godot.ButtonList.Right)
				{
					Vector2 rel1 = event1.Position - _last_mouse_press.Position;
					Vector2 rel2 = rel1 + event1.Relative;
					Godot.Collections.Dictionary mult_drag_dict = new Godot.Collections.Dictionary();
					mult_drag_dict["Position"] = event1.Position;
					mult_drag_dict["Relative"] = rel1.AngleTo(rel2);
					mult_drag_dict["Speed"] = event1.Speed;
					Emit(nameof(Twist), new InputEventScreenTwist(mult_drag_dict));
				}
			}
		}
		else if(@event is InputEventScreenTouch event2)
		{
			if(event2.Pressed)
			{
				_touches[event2.Index] = event2;
				if(event2.Index == 0)
				{
					Emit(nameof(SingleTouch), new InputEventSingleScreenTouch(event2));
					OnlyTouch = event2;
					if(TapDelayTimer.IsStopped())
					{
						TapDelayTimer.Start(TOUCH_DELAY_TIME);
					}
				}
				else
				{
					OnlyTouch = null;
					_CancelSingleDrag();
				}
			}
			else
			{
				if(event2.Index == 0 && OnlyTouch != null)
				{
					Emit(nameof(SingleTouch), new InputEventSingleScreenTouch(event2));
				}
				_touches.Remove(event2.Index);
				_drags.Remove(event2.Index);
				_CancelSingleDrag();
			}
		}
		else if(@event is InputEventScreenDrag event3)
		{
			_drags[event3.Index] = event3;
			if(!_ComplexGestureInProgress())
			{
				if(_drag_enabled)
				{
					Emit(nameof(SingleDrag), new InputEventSingleScreenDrag(event3));
				}
				else
				{
					if(DragStartupTimer.IsStopped())
					{
						DragStartupTimer.Start(DRAG_STARTUP_TIME);
					}
				}
			}
			else
			{
				_CancelSingleDrag();
				Gesture gesture = _IdentifyGesture(_drags);
				switch(gesture)
				{
					case Gesture.PINCH:
					{
						Emit(nameof(Pinch), new InputEventScreenPinch(_drags));
						break;
					}
					case Gesture.MULTI_DRAG:
					{
						Emit(nameof(MultiDrag), new InputEventMultiScreenDrag(_drags));
						break;
					}
					case Gesture.TWIST:
					{
						Emit(nameof(Twist), new InputEventScreenTwist(_drags));
						break;
					}
					default:
					{
						break;
					}
				}
			}
		}
	}
}
