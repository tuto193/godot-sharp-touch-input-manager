# Godot-Sharp-Touch-Input-Manager

Just a **C#-based** re-implementation of [Godot-Touch-Input-Manager](https://github.com/Federico-Ciuffardi/Godot-Touch-Input-Manager). Don't  personally know if there are any actual performance gains, but I had nothing else to do during this pandemic.

For the general properties, please reffer to [Federico-Ciuffardi's source](https://github.com/Federico-Ciuffardi/Godot-Touch-Input-Manager/blob/master/README.md).

## General Use
*  Autoload it.
*  Since there are no static members, make sure to always `GetNode<InputManager>("/root/InputManager");` after autoloading it in ordere to connect its signals to your methods.


## Godot Version
Since I programmed it in  Godot 3.2.1, that's the only version I can honestly recommend.
Prior versions might work, and forward compatibility is not a promise.

## Authors
*  Federico Ciuffardi
*  Carlos Alfonso Parra Fernandez

Like Federico said, feel free to append yourself to the list, in case you also contributed.

## Note
For more questions or suggestions write me at `carlos.a.parra.f@gmail.com`. For other stuff don't forget to contact the real owner, since all credit goes to him. I just ported the code out of boredom.